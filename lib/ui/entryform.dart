import 'package:ijk_shoes/appBar.dart';
import 'package:flutter/material.dart';
import 'package:ijk_shoes/models/contact.dart';
import 'package:flutter/cupertino.dart';
import 'package:ijk_shoes/ui/viewDaftarProduk.dart';
import 'package:ijk_shoes/colorPick.dart';

class EntryForm extends StatefulWidget {
  @override
  EntryFormState createState() => EntryFormState();
}

//class controller
class EntryFormState extends State<EntryForm> {
  Contact contact;

  // EntryFormState(this.contact);

  TextEditingController nameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController deskripsiController = TextEditingController();
  TextEditingController fotoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //kondisi
    if (contact != null) {
      nameController.text = contact.name;
      usernameController.text = contact.username;
      deskripsiController.text = contact.deskripsi;
      fotoController.text = contact.foto;
    }
    //rubah
    return SafeArea(
        child: Scaffold(
            appBar: AllAppBar(),
            body: Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
              child: ListView(
                children: <Widget>[
                  // username
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: usernameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Nama Produk',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  // nama
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: nameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Harga Produk',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // deskripsi
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: deskripsiController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Description',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // foto
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: fotoController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Photo Url',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  new InkWell(
                    onTap: () {
                      // Navigator.push(
                      //     context, MaterialPageRoute(builder: (context) => LauncherPage()));
                      if (contact == null) {
                        // tambah data
                        contact = Contact(
                            nameController.text,
                            usernameController.text,
                            deskripsiController.text,
                            fotoController.text);
                      } else {
                        // ubah data
                        contact.name = nameController.text;
                        contact.username = usernameController.text;
                        contact.deskripsi = deskripsiController.text;
                        contact.foto = fotoController.text;
                      }
                      // kembali ke layar sebelumnya dengan membawa objek contact
                      navigateToEntryForm(context, contact);
                    },
                    child: Container(
                      width: 200,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade200,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [
                                Colors.amberAccent,
                                Colors.orangeAccent
                              ])),
                      child: Text(
                        'Tambah Produk',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}

Future<Contact> navigateToEntryForm(
    BuildContext context, Contact contact) async {
  var result = await Navigator.push(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return ViewDaftarProduk();
  }));
  return result;
}
