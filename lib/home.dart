import 'package:ijk_shoes/appBar.dart';
import 'package:ijk_shoes/colorPick.dart';
import 'package:flutter/material.dart';
import 'package:ijk_shoes/viewHome.dart';

import 'models/viewModel.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    getDataUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        appBar: AllAppBar(),
        body: _gridView(),
        backgroundColor: Colors.indigo[50],
      ),
    );
  }

  Widget _gridView() {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        itemCount: dataUser.length,
        itemBuilder: (context, i) {
          return Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      dataUser[i]['gambar'],
                      cacheHeight: 115,
                      cacheWidth: 115,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      dataUser[i]['nama'] ?? "Fossil",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      dataUser[i]['harga'] ?? "Rp. 900.499",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              )),
            ),
          );
        });
  }
}
