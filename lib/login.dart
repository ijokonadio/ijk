import 'package:ijk_shoes/navbar.dart';
import 'package:flutter/material.dart';
import 'package:ijk_shoes/registrasi.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String src = "";
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
          backgroundColor: Colors.deepOrange,
          radius: 80,
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Icon(
                Icons.account_balance,
                size: 100,
                color: Colors.black,
              ),
              // Image.network(
              //   src,
              //   cacheHeight: 100,
              //   cacheWidth: 100,
              // ),
              Text(
                "IJK SHOES",
                style: TextStyle(color: Colors.black),
              )
            ],
          )),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context)
              .pushReplacement(new MaterialPageRoute(builder: (_) {
            return new BelajarNavBar();
          }));
        },
        padding: EdgeInsets.all(12),
        color: Colors.deepOrange,
        child: Text('Log In', style: TextStyle(color: Colors.black)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );
    final regisLabel = FlatButton(
      child: Text(
        "Don't have an account? sign up",
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        Navigator.of(context)
            .pushReplacement(new MaterialPageRoute(builder: (_) {
          return new Registrasi();
        }));
      },
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 24.0),
            loginButton,
            forgotLabel,
            regisLabel
          ],
        ),
      ),
    );
  }
}
