import 'package:ijk_shoes/appBar.dart';
import 'package:ijk_shoes/colorPick.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      body: Center(
        child: Container(
            padding: EdgeInsets.all(15),
            alignment: Alignment.bottomCenter,
            color: Warna.grey,
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "Copyright MOBILE PROGRAMING II Maulana malik aziz, 18282009",
                    style: TextStyle(color: Colors.black, fontSize: 17),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    "IJK SHOES",
                    style: TextStyle(color: Colors.black, fontSize: 17),
                  ),
                )
              ],
            )),
      ),
    ));
  }
}
