import 'package:flutter/material.dart';
import 'package:ijk_shoes/appBar.dart';
import 'package:ijk_shoes/colorPick.dart';

class ViewHome extends StatefulWidget {
  ViewHome({Key key}) : super(key: key);

  @override
  _ViewHomeState createState() => _ViewHomeState();
}

class _ViewHomeState extends State<ViewHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AllAppBar(),
      body: _grid(),
    );
  }

  Widget _grid() {
    return new Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: GridView.count(
        childAspectRatio: 8.0 / 9.0,
        shrinkWrap: true,
        mainAxisSpacing: 20,
        crossAxisCount: 2,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn1-production-images-kly.akamaized.net/SPsPZIwx8jHkiGuv9ybmowycmEw=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3128303/original/025341400_1589454782-parker-burchfield-tvG4WvjgsEY-unsplash.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Baju Polos",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 20000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://4.bp.blogspot.com/-Zo1Xpc9YHe4/WBqbO8GNNFI/AAAAAAAA0us/SF-QXNQkA5Y-7j8lSwucv-oU3NqhlwE-ACLcB/s1600/Contoh%2BModel%2BBaju%2BMuslim%2BTunik%2BModern%2BTerbaru%2B2017.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Tunik Wanita",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 150.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://lh3.googleusercontent.com/-TUos_Pxmo88/WheATaDjifI/AAAAAAAAPhs/osTcVhSyGHcaTaC7eQ-X5qvRaa_xI-aNgCHMYCw/s1600/IMG-20171123-WA0027.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Gamis Wanita",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 300.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://fasnina.com/wp-content/uploads/2018/11/gamis-pria-warna-hijau.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Gamis Pria",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 275.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/5/31/720549607/720549607_5fdc325d-ba7e-469c-a8f9-d83b21d5e3d4_1956_1956.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Jeans sobek Pria",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 200.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://s3.bukalapak.com/img/8342016713/s-1000-1000/Celana_Kulot_Panjang_Wanita_Bahan_Katun_Rami_Termurah.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Jeans Kulot",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 125.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
        ],
      ),
    );
  }

  Widget _produk(String nama, String harga, String url) {
    return new Container(
        color: Warna.grey,
        child: Card(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.network(
                  url,
                  // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                height: 100,
                margin: EdgeInsets.only(left: 8),
                child: Column(
                  children: [
                    Text(
                      nama,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black87,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.ac_unit_rounded,
                          color: Colors.lightBlueAccent,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Snow Furniture",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Warna.biruMuda,
                              backgroundColor: Colors.transparent),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                height: 100,
                child: Text(
                  harga,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ));
  }
}
