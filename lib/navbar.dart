import 'package:ijk_shoes/about.dart';
import 'package:ijk_shoes/colorPick.dart';
import 'package:ijk_shoes/home.dart';
import 'package:ijk_shoes/models/contact.dart';
import 'package:ijk_shoes/produk.dart';
import 'package:flutter/material.dart';
import 'package:ijk_shoes/ui/entryform.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';

  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [new BerandaPage(), EntryForm(), About()];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
            activeIcon: new Icon(
              Icons.account_balance_rounded,
              color: Colors.orange,
            ),
            icon: new Icon(
              Icons.account_balance_rounded,
              color: Colors.grey,
            ),
            title: new Text('Home', style: TextStyle(color: Colors.black))),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.add_outlined,
            color: Colors.orange,
          ),
          icon: new Icon(
            Icons.add_outlined,
            color: Colors.grey,
          ),
          title: new Text('Add shoes', style: TextStyle(color: Colors.black)),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.chrome_reader_mode_rounded,
            color: Colors.orange,
          ),
          icon: new Icon(
            Icons.chrome_reader_mode_rounded,
            color: Colors.grey,
          ),
          title: new Text('About', style: TextStyle(color: Colors.black)),
        ),
      ],
    );
  }
}
